import React from "react";

import { navigationRef } from "~/services/NavigationService";

import AntDesign from "react-native-vector-icons/AntDesign";
import colors from "~/styles/colors";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

AntDesign.loadFont();

const Stack = createStackNavigator();

import Login from "~/screens/Login";
import Datasets from "~/screens/Datasets";
import Documents from "~/screens/Documents";

import { StatusBar, View, Text, TouchableOpacity } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { setEmail } from "~/store/modules/auth/actions";

export default function Routes() {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  return (
    <>
      <StatusBar backgroundColor={`${colors.primary}DD`} />
      <NavigationContainer ref={navigationRef}>
        {auth.email && (
          <Stack.Navigator>
            <Stack.Screen
              name="Datasets"
              component={Datasets}
              options={{
                title: "Datasets",
                headerTintColor: colors.white,
                headerStyle: {
                  backgroundColor: colors.primary,
                },
                headerRight: () => (
                  <View
                    style={{
                      marginRight: 20,
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <TouchableOpacity
                      style={{ marginRight: 15 }}
                      onPress={() => null}
                    >
                      <AntDesign name="plus" size={25} color={colors.white} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => dispatch(setEmail(null))}>
                      <AntDesign name="logout" size={20} color={colors.white} />
                    </TouchableOpacity>
                  </View>
                ),
              }}
            />

            <Stack.Screen
              name="Documents"
              component={Documents}
              options={{
                title: "Documents",
                headerTintColor: colors.white,
                headerStyle: {
                  backgroundColor: colors.primary,
                },
              }}
            />
          </Stack.Navigator>
        )}

        {!auth.email && (
          <Stack.Navigator>
            <Stack.Screen
              name="Login"
              component={Login}
              options={{
                title: "Login",
                headerTintColor: colors.white,
                headerStyle: {
                  backgroundColor: colors.primary,
                },
              }}
            />
          </Stack.Navigator>
        )}
      </NavigationContainer>
    </>
  );
}
