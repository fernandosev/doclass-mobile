import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  width: 120px;
  height: 40px;
  border-radius: 500px;
  background-color: ${(props) => props.background};
  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text`
  font-size: 18px;
  color: ${(props) => props.color};
`;
