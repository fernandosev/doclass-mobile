import axios from "axios";

export const { CancelToken, isCancel } = axios;

const api = axios.create({
  baseURL: "http://192.168.2.186:5000",
});
export default api;
