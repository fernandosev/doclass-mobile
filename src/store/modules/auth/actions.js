export function setEmail(email) {
  return {
    type: "@auth/SET_EMAIL",
    payload: { email },
  };
}

export function newSession() {
  return {
    type: "@auth/NEW_SESSION",
  };
}
