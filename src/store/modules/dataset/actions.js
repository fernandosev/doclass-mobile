export function datasetsRequest() {
  return {
    type: '@dataset/DATASETS_REQUEST',
  };
}

export function datasetsSuccess(data) {
  return {
    type: '@dataset/DATASETS_SUCCESS',
    payload: {data},
  };
}

export function datasetsFailure() {
  return {
    type: '@dataset/DATASETS_FAILURE',
  };
}

export function setDataset(dataset) {
  return {
    type: '@dataset/SET_DATASET',
    payload: {dataset},
  };
}
