export function classifyRequest(_id) {
  return {
    type: "@document/CLASSIFY_REQUEST",
    payload: { _id },
  };
}

export function classifySuccess() {
  return {
    type: "@document/CLASSIFY_SUCCESS",
  };
}

export function classifyFailure() {
  return {
    type: "@document/CLASSIFY_FAILURE",
  };
}

export function documentsRequest(size) {
  return {
    type: "@document/DOCUMENTS_REQUEST",
    payload: { size },
  };
}

export function clear() {
  return {
    type: "@document/CLEAR",
  };
}

export function documentsSuccess(documents, total) {
  return {
    type: "@document/DOCUMENTS_SUCCESS",
    payload: { documents, total },
  };
}

export function documentsFailure() {
  return {
    type: "@document/DOCUMENTS_FAILURE",
  };
}

export function documentRequest(_id, label) {
  return {
    type: "@document/DOCUMENT_REQUEST",
    payload: { _id, label },
  };
}

export function documentSuccess(document) {
  return {
    type: "@document/DOCUMENT_SUCCESS",
    payload: { document },
  };
}

export function documentFailure() {
  return {
    type: "@document/DOCUMENT_FAILURE",
  };
}
