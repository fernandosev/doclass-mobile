import produce from "immer";

const INITIAL_STATE = {
  documentsLoading: false,
  documentLoading: false,
  classifyLoading: false,
  index: 0,
  total: 0,
  documents: [],
  document: {},
};

export default function document(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case "@document/CLASSIFY_REQUEST": {
        draft.classifyLoading = true;
        break;
      }

      case "@document/CLASSIFY_SUCCESS": {
        draft.classifyLoading = false;
        break;
      }

      case "@document/CLASSIFY_FAILURE": {
        draft.classifyLoading = false;
        break;
      }

      case "@document/DOCUMENTS_REQUEST": {
        draft.documentsLoading = true;
        break;
      }

      case "@document/DOCUMENTS_SUCCESS": {
        draft.documents = [...draft.documents, ...action.payload.documents];
        draft.total = action.payload.total;
        draft.index += action.payload.documents.length;
        draft.documentsLoading = false;
        break;
      }

      case "@document/CLEAR": {
        draft.documents = [];
        draft.index = 0;
        draft.total = 0;
        break;
      }

      case "@document/DOCUMENTS_FAILURE": {
        draft.documentsLoading = false;
        break;
      }

      case "@document/DOCUMENT_REQUEST": {
        draft.documentLoading = true;
        break;
      }

      case "@document/DOCUMENT_SUCCESS": {
        draft.document = action.payload.document;
        draft.documentLoading = false;
        break;
      }

      case "@document/DOCUMENT_FAILURE": {
        draft.documentLoading = false;
        break;
      }
    }
  });
}
