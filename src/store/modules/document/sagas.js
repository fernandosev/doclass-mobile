import { takeLatest, call, put, all, select } from "redux-saga/effects";
import api from "~/config/api";
import {
  documentsFailure,
  documentsSuccess,
  documentFailure,
  documentSuccess,
} from "./actions";

function* documents({ payload }) {
  const datasetID = yield select((state) => state.dataset.dataset._id);
  const index = yield select((state) => state.document.index);
  const { size } = payload;

  try {
    const total = yield call(api.get, `/document/dataset/${datasetID}/count`);
    const response = yield call(
      api.get,
      `/document/list/${index}/${size}/from/${datasetID}`
    );

    yield put(documentsSuccess(response.data, total.data));
  } catch (err) {
    yield put(documentsFailure());
    console.log(err);
  }
}

function* document({ payload }) {
  const { _id, label } = payload;

  try {
    const response = yield call(api.get, `/document/content/${_id}`);

    yield put(
      documentSuccess({
        content: response.data,
        _id,
        label,
      })
    );
  } catch (err) {
    yield put(documentFailure());
    console.log(err);
  }
}

export default all([
  takeLatest("@document/DOCUMENTS_REQUEST", documents),
  takeLatest("@document/DOCUMENT_REQUEST", document),
]);
