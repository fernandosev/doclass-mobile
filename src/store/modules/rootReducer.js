import { combineReducers } from "redux";

import auth from "./auth/reducer";
import network from "./network/reducer";
import dataset from "./dataset/reducer";
import document from "./document/reducer";

export default combineReducers({
  auth,
  network,
  dataset,
  document,
});
